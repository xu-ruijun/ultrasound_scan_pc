#!/usr/bin/python3
from rx import UsRx

import matplotlib.pyplot as plt
import numpy as np


t = 5e-6

if __name__ == '__main__':
    fig = plt.figure()
    ax = fig.add_subplot(111, polar=True)
    θ = np.linspace(0, 2*np.pi, 64)
    ρ = np.arange(4096)*(t/2*340)

    data = np.zeros((64, 4096))
    i = 0
    rx = UsRx()
    rx.start()
    while True:
        ax.cla()
        data[i] = rx.queue.get()
        ax.pcolormesh(θ, ρ[:2000], data.transpose()[:2000], shading='auto', vmin=0, vmax=200)
        i = (i+rx.rot//32)%64
        plt.pause(0.01)