# ultrasound_scan_pc

#### 介绍
超声波旋转扫描成像上位机  
HC-SR04模块引出模拟信号，STM32采集后发送到电脑，进行脉冲压缩。加上步进电机，在极坐标下画出回波图。

#### 软件架构
USB接收 > 旋转方向同步  
v  
转换为numpy数组  
v  
脉冲压缩:FFT后乘系数再IFFT  
v  
希尔伯特变换包络检测  
v  
matplotlib实时绘图  


#### 文件说明
| 文件名   |   说明  |
| ------  | ------- |
| rx.py   | 数据接收 |
| mcys.py | 脉冲压缩 |
| test.py | 一维实时测试 |
| polar.py | 极坐标实时绘图 |


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  获取用于脉冲压缩的回波样本，保存为save1.npy
2.  运行test.py或polar.py使用

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
