#!/usr/bin/python3
from rx import UsRx

import numpy as np
import matplotlib.pyplot as plt


t = 5e-6

if __name__ == '__main__':
    x = np.arange(4096)*(t/2*340)
    rx = UsRx()
    rx.start()
    while True:
        frame = rx.queue.get()
        plt.cla()
        plt.ylim(0, 1000)
        plt.plot(x[:frame.shape[0]], frame)
        plt.pause(0.01)
